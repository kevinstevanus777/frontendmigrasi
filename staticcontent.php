<?php


$selectkabupaten = query("SELECT * FROM kabupaten");
$selectberita = query("SELECT * FROM berita ORDER BY RAND() LIMIT 3");
$selectheadlineberita = query("SELECT * FROM berita ORDER BY RAND() LIMIT 1")[0];
$selectslider = query("SELECT * FROM slider");

$selectkegiatan = query("SELECT * FROM kegiatan ORDER BY RAND() LIMIT 3");
?>

<!-- AKhir menu -->
<!--slider-->
<!--namanya carousel di bootstrap 3.7-->

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php
        $i = 0;
        foreach ($selectslider as $row) {
            $actives = '';
            if ($i == 0) {
                $actives = 'active';
            }
            ?>
            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $actives; ?>"></li>
        <?php $i++;
        } ?>
    </ol>

    <!-- The slideshow -->
    <div class="carousel-inner" role="listbox">
        <?php
        $i = 0;
        foreach ($selectslider as $row) {
            $actives = '';
            if ($i == 0) {
                $actives = 'active';
            }
            ?>
            <div class="item <?php echo $actives ?> ">
                <img src="pbwmigrasi/img/<?php echo $row['sliderFOTO'] ?>" style="width:100%; height:300px">
            </div>
        <?php $i++;
        } ?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

</div>



<!--akhir menu -->

<!--membuat kolom berita 2kolom -->

<div class="jumbotron">
    <div class="container">

        <a href="isiberita.php?beritaKODE=<?= $selectheadlineberita["beritaKODE"]; ?>">
            <h1><?= $selectheadlineberita["beritaJUDUL"]; ?></h1>
        </a>

        <p><?= $selectheadlineberita["beritaISI"]; ?></p>
        <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p> -->
    </div>
</div>


<div class="container">

    <div class="row">

        <div class="col-sm-8">
            <?php foreach ($selectberita as $berita) : ?>
                <!--namanya media heading (media objek di bootstap)-->
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" style="width:150px;height:150px" ; src="pbwmigrasi/img/<?= $berita["beritaICONFOTO"]; ?>" alt="tidak ada">
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="isiberita.php?beritaKODE=<?= $berita["beritaKODE"]; ?>">
                            <h4 class="media-heading"><?= $berita["beritaJUDUL"]; ?></h4>
                        </a>

                        <!-- <h1> Indonesia</h1> -->
                        <?= $berita["beritaISI"]; ?>
                        <!--https://bootsnipp.com/snippets/8yNX-->

                    </div>
                </div>
                <!--2-->


            <?php endforeach; ?>
        </div>

        <!--list group nootstrap components  (custom contents-->
        <div class="col-sm-4">


            <?php foreach ($selectkegiatan as $kegiatan) : ?>

                <div class="list-group">
                    <a href="#" class="list-group-item active">
                        <h4 class="list-group-item-heading"><?= $kegiatan["eventNAMA"]; ?></h4>
                        <p><?= $kegiatan["eventKET"]; ?> </p>
                        <p class="list-group-item-text"><?= $kegiatan["eventMULAI"]; ?></p>
                    </a>
                </div>

            <?php endforeach; ?>
            <!-- <div class="list-group">
                <a href="#" class="list-group-item active">
                    <h4 class="list-group-item-heading">List group item heading</h4>
                    <p>Lorem ipsum, or lipsum as it is sometimes known </p>
                    <p class="list-group-item-text">Best</p>
                </a>
            </div>
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    <h4 class="list-group-item-heading">List group item heading</h4>
                    <p>Lorem ipsum, or lipsum as it is sometimes known </p>
                    <p class="list-group-item-text">Best</p>
                </a>
            </div> -->
        </div>
    </div>
</div>

<!--galeri foto-->
<!--https://bootsnipp.com/snippets/8yNX  (file dibawah buka dri link ini)html-->
<script src="//rawgithub.com/ashleydw/lightbox/master/dist/ekko-lightbox.js"></script>

<div class="container mt40">
    <section class="row">


        <?php foreach ($selectkabupaten as $kabupaten) : ?>
            <article class="col-xs-12 col-sm-6 col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="pbwmigrasi/img/<?= $kabupaten["kabupatenFOTOICON"]; ?>" title="Nature Portfolio" class="zoom" data-title="Amazing Nature" data-footer="The beauty of nature" data-type="image" data-toggle="lightbox">
                            <img src="pbwmigrasi/img/<?= $kabupaten["kabupatenFOTOICON"]; ?>" style="height:150px" alt="Nature background" />
                            <span class="overlay"><i class="glyphicon glyphicon-fullscreen"></i></span>
                        </a>
                    </div>
                    <div class="panel-footer">
                        <h4><a href="listobyekwisata.php?kabupatenKODE=<?= $kabupaten["kabupatenKODE"]; ?>" title="Alam"><?= $kabupaten["kabupatenNAMA"]; ?></a></h4>
                        <!-- <span class="pull-right">
                            <i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                            <div id="like1-bs3"></div>
                            <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                            <div id="dislike1-bs3"></div>
                        </span> -->
                    </div>
                </div>

            </article>

        <?php endforeach; ?>




    </section>
</div>