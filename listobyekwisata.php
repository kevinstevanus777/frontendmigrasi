<?php

include "_header.php";

$id = $_GET["kabupatenKODE"];
// $selectkecamatan = query("SELECT * FROM kecamatan WHERE kabupatenKODE = '$id'");


$selectdestinasiwisata = query("SELECT * 
                                FROM obyekwisata ow
                                WHERE ow.kecamatanKODE IN (SELECT kec.kecamatanKODE
                                FROM kabupaten kab, kecamatan kec
                                WHERE kab.kabupatenKODE = kec.kabupatenKODE AND kab.kabupatenKODE = '$id'
)");




$selectkabupaten = query("SELECT * FROM kabupaten where kabupatenKODE = '$id'")[0];
// $selectberita = query("SELECT * FROM berita ORDER BY RAND() LIMIT 3");
// $selectheadlineberita = query("SELECT * FROM berita ORDER BY RAND() LIMIT 1")[0];
// $selectslider = query("SELECT * FROM slider");

// $selectkegiatan = query("SELECT * FROM kegiatan ORDER BY RAND() LIMIT 3");
?>




<div class="container">

    <div class="jumbotron">
        <div class="container">

            <h1><?= $selectkabupaten["kabupatenNAMA"]; ?></h1>
            <!-- <p><?= $selectkabupaten["kabupatenFOTOICONKET"]; ?></p> -->
            <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p> -->
        </div>
    </div>

    <div class="container mt40">
        <section class="row">


            <?php foreach ($selectdestinasiwisata as $destinasiwisata) : ?>
                <article class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <a href="pbwmigrasi/img/<?= $destinasiwisata["obyekFOTO"]; ?>" title="Nature Portfolio" class="zoom" data-title="Amazing Nature" data-footer="The beauty of nature" data-type="image" data-toggle="lightbox">
                                <img src="pbwmigrasi/img/<?= $destinasiwisata["obyekFOTO"]; ?>" style="height:150px" alt="Nature background" />
                                <span class="overlay"><i class="glyphicon glyphicon-fullscreen"></i></span>
                            </a>
                        </div>
                        <div class="panel-footer">
                            <h4><a href="detailobyekwisata.php?obyekKODE=<?= $destinasiwisata["obyekKODE"]; ?>" title="Alam"><?= $destinasiwisata["obyekALAMAT"]; ?></a></h4>
                            <!-- <span class="pull-right">
                            <i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                            <div id="like1-bs3"></div>
                            <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                            <div id="dislike1-bs3"></div>
                        </span> -->
                        </div>
                    </div>

                </article>

            <?php endforeach; ?>




        </section>
    </div>



</div>

<!--galeri foto-->
<!--https://bootsnipp.com/snippets/8yNX  (file dibawah buka dri link ini)html-->
<script src="//rawgithub.com/ashleydw/lightbox/master/dist/ekko-lightbox.js"></script>



<?php

include "_footer.php";

?>